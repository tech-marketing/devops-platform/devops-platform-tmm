from flask import Flask, render_template
from prometheus_flask_exporter import PrometheusMetrics
import mysql.connector
from datetime import date
from mysql.connector import errorcode
from sql_queries import *

# Flask app object
app = Flask(__name__)
metrics = PrometheusMetrics(app)
metrics.info('app_info', 'Application info', version='1.0.0')
# Endpoint
@app.route('/')
def index():
    return render_template('index.html')
    
"""
@app.route('/metrics')
def index_metrics():
    return render_template('index.html')
"""

@app.route('/test')
def visitor_counter_test():
    counter_select = read_counter
    print("counter so far")
    return counter_select


@app.route('/db')
def visitor_counter():
  pass


if __name__ == "__main__":
    app.run(host='0.0.0.0', port='8081')

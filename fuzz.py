from pythonfuzz.main import PythonFuzz
import sys 

@PythonFuzz
def fuzz(buf):
    try:
        string = buf.decode("ascii")
        if len(string) == 1 and string[0]=='f':
            #raise Exception('nice')
            sys.exit(0)
    except UnicodeDecodeError:
        pass

if __name__ == '__main__':
    fuzz()

